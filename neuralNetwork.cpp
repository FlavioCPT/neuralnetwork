/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: Final Project
 * Purpose: Network class implementations
 * Notes:
 * *************************************************************************/

#include "neuralNetwork.h"

//- - - - - - - - - - - - - - - - - -
//   ***RANDOM NUMBER GENERATOR***
//- - - - - - - - - - - - - - - - - -
float NeuralNetwork::randomWeight(){
    float weightValue;
    int MAXInWantedRange;
    int MINInWantedRange;

    //To make a random weight between "1" and "-1": (1)Divide by the highest number that "rand()"
    //is able to produce, which will yield a fraction between "0" and "1". (2)This produced fraction is then multiplied
    //by "2" meaning that the random number now ranges from "0" to "2". (3)Finally, the range is moved by subtracting by "1"
    //thus moving the final number into a range of "-1" to "1".
    if((on == 1.0) && (off == 0.0)){
        MAXInWantedRange = 1;
        MINInWantedRange = 0;
        weightValue = float(rand()) / float(RAND_MAX);
    }
    else if((on == 1.0) && (off == -1.0)){
        MAXInWantedRange = 2;
        MINInWantedRange = abs(-1);
        weightValue = (float(rand()) / float(RAND_MAX) * MAXInWantedRange) - MINInWantedRange;
    }
    else{weightValue = 0;};

    return weightValue;
}

//- - - - - - - - - - - - - - - - - - - - - -
//   ***GRAB DATA FROM THE CONFIG FILE***
//- - - - - - - - - - - - - - - - - - - - - -
void NeuralNetwork::loadCfgParams(){
    //Variables needed to read the file.
    fstream configFile;
    string fileLine;
    char fileChar;
    int significantLinesInFile = 0;

    //The ".txt" file is opened for reading only.
    configFile.open("config.txt", ios::in);

    //File open operation is checked.
    if (!configFile) {
        cout << "ERROR: Action to open file failed." << endl;
    }else{
        //First, the file is traversed with the purpose of counting all lines that
        //are not '\n' or the 'EOF' element. This integer is then stored
        //in the "significantLinesInFile" variable.
        fileChar = configFile.peek();
        while(configFile){
            getline(configFile, fileLine, '\n');
            fileChar = configFile.peek();
            if(fileChar == EOF){configFile.get();};
            significantLinesInFile++;
        };
        significantLinesInFile--;

        //The config file's line count is checked to see if the file
        //is set up appropriately in terms of line count.
        if(significantLinesInFile >= 11){
            cout << "ERROR: The \"config.txt\" file has been modified. Revert the changes." << endl;
        }else{
            //Rewinding file to the beginning.
            configFile.clear();
            configFile.seekg(0L, ios::beg);

            //The config file's contents are assigned to the class private
            //member variables.
            float strToFlo;
            int strToInt;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            on = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            off = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            onSoft = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            offSoft = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToInt = stoi(fileLine);
            inUnits = strToInt;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToInt = stoi(fileLine);
            hidUnits = strToInt;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToInt = stoi(fileLine);
            outUnits = strToInt;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToInt = stoi(fileLine);
            maxEpoch = strToInt;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            learnRate = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            learnRate = strToFlo;
            getline(configFile, fileLine, ' ');
            getline(configFile, fileLine, '\n');
            strToFlo = stof(fileLine);
            ee = strToFlo;

            //The ".txt" file is then closed.
            configFile.close();
        };
    };
}


//- - - - - - - - - - - - - - - - - - - - - - -
//   ***GRAB DATA FROM THE TRAINING FILE***
//- - - - - - - - - - - - - - - - - - - - - - -
void NeuralNetwork::buildIOData(){
    //Variables needed to read the file.
    fstream iOFile;
    string fileLine;
    char fileChar;
    ioPairs = 0;

    //The ".txt" file is opened for reading only.
    iOFile.open("trainingData.txt", ios::in);

    //File open operation is checked.
    if (!iOFile){
        cout << "ERROR: Action to open file failed." << endl;
    }else{
        fileChar = iOFile.peek();
        while(iOFile){
            getline(iOFile, fileLine, '\n');
            fileChar = iOFile.peek();
            if(fileChar == EOF){iOFile.get();};
            ioPairs++;
        };
        ioPairs = ioPairs / 2;

        //Rewinding file to the beginning.
        iOFile.clear();
        iOFile.seekg(0L, ios::beg);

        //The pointer to pointer variable "inputData", is prepared as a
        //2-D array before it may be used.
        inputData = new float *[ioPairs];
        for(int index = 0; index < ioPairs; index++){
            inputData[index] = new float[2];
        };
        for(int outerLoop = 0; outerLoop < ioPairs; outerLoop++){
            for(int innerLoop = 0; innerLoop < 2; innerLoop++){
                inputData[outerLoop][innerLoop] = 0;
            };
        };

        //The 2-D array is populated with the .txt file's desired input data.
        for(int iteration = 0; iteration < ioPairs; iteration++){
            getline(iOFile, fileLine, ' ');
            inputData[iteration][0] = stof(fileLine);
            getline(iOFile, fileLine, '\n');
            inputData[iteration][1] = stof(fileLine);
        };

        //The pointer to pointer variable "outputData", is prepared as a
        //2-D array before it may be used.
        outputData = new float *[ioPairs];
        for(int index = 0; index < ioPairs; index++){
            outputData[index] = new float[2];
        };
        for(int outerLoop = 0; outerLoop < ioPairs; outerLoop++){
            for(int innerLoop = 0; innerLoop < 2; innerLoop++){
                outputData[outerLoop][innerLoop] = 0;
            };
        };

        //The 2-D array is populated with the .txt file's desired output data.
        for (int iteration = 0; iteration < ioPairs; iteration++){
            getline(iOFile, fileLine, '\n');
            outputData[iteration][0] = stof(fileLine);
        };

        //The ".txt" file is now closed.
        iOFile.close();
    }
}

//- - - - - - - - - - - - - - - - - -
//   ***BUILD THE INPUT LAYER***
//- - - - - - - - - - - - - - - - - -
void NeuralNetwork::buildInputLayer(){
    //The 1-D array is built to hold the given input values from the training file.
    //The bias node's value is the last cell in the array.
    nNlayersPtr->inputLayer.neurons = new float[(inUnits + 1)];
    for(int index = 0;index < inUnits; index++){
        nNlayersPtr->inputLayer.neurons[index] = 0;
    };
    nNlayersPtr->inputLayer.neurons[inUnits] = on;

    //A 2-D array is built to hold the weight values from the input layer to the hidden layer.
    //The bias node's weights are the last row in the array.
    nNlayersPtr->inputLayer.weights = new float *[(inUnits + 1)];
    for(int index = 0; index < (inUnits + 1); index++){
        nNlayersPtr->inputLayer.weights[index] = new float[hidUnits];
    };
    for(int outerLoop = 0; outerLoop < (inUnits + 1); outerLoop++){
        for(int innerLoop = 0; innerLoop < hidUnits; innerLoop++){
            nNlayersPtr->inputLayer.weights[outerLoop][innerLoop] = randomWeight();
        };
    };
}

//- - - - - - - - - - - - - - - - - -
//   ***BUILD THE HIDDEN LAYER***
//- - - - - - - - - - - - - - - - - -
void NeuralNetwork::buildHiddenLayer(){
    //The 1-D array is built to hold the given input values from the training file.
    //The array will be as big as the value from the config file dictates. The bias node's
    //value is the last cell in the array.
    nNlayersPtr->hiddenLayer.neurons = new float[(hidUnits + 1)];
    for(int index = 0;index < hidUnits; index++){
        nNlayersPtr->hiddenLayer.neurons[index] = 0;
    };
    nNlayersPtr->hiddenLayer.neurons[hidUnits] = on;

    //A 2-D array is built to hold the weight values from the hidden layer to the output layer.
    //The bias node's weights are the last row in the array. In this project, this 2-D array is
    //really being used as a 1-D array.
    nNlayersPtr->hiddenLayer.weights = new float *[(hidUnits + 1)];
    for(int index = 0; index < (hidUnits + 1); index++){
        nNlayersPtr->hiddenLayer.weights[index] = new float[outUnits];
    };
    for(int outerLoop = 0; outerLoop < (hidUnits + 1); outerLoop++){
        for(int innerLoop = 0; innerLoop < hidUnits; innerLoop++){
            nNlayersPtr->hiddenLayer.weights[outerLoop][innerLoop] = randomWeight();
        };
    };

    //A 1-D array is built to hold the error values belonging to the designated hidden layer neurons.
    //The bias node in this layer will also have an error.
    nNlayersPtr->hiddenLayer.errors = new float[(hidUnits + 1)];
    for(int index = 0;index < hidUnits; index++){
        nNlayersPtr->hiddenLayer.errors[index] = 0;
    };
}

//- - - - - - - - - - - - - - - - - -
//   ***BUILD THE OUTPUT LAYER***
//- - - - - - - - - - - - - - - - - -
void NeuralNetwork::buildOutputLayer(){
    //A 1-D array is built to hold the given output values after any propagated activations.
    //The there is no bias node in this layer.
    nNlayersPtr->outputLayer.neurons = new float[outUnits];
    for(int index = 0;index < outUnits; index++){
        nNlayersPtr->outputLayer.neurons[index] = 0;
    };

    //This 1-D array is built to hold the error values belonging to output layer's neuron(s).
    nNlayersPtr->outputLayer.errors = new float[outUnits];
    for(int index = 0;index < outUnits; index++){
        nNlayersPtr->outputLayer.errors[index] = 0;
    };
}

//- - - - - - - - - - - - - - - -
//   ***ASSIGN ACTIVATIONS***
//- - - - - - - - - - - - - - - -
void NeuralNetwork::assignActivatons(int dataSetPair){
    nNlayersPtr->inputLayer.neurons[0] = inputData[dataSetPair][0];
    nNlayersPtr->inputLayer.neurons[1] = inputData[dataSetPair][1];
}

//- - - - - - - - - - - - - - - - -
//   ***PROPAGATE ACTIVATIONS***
//- - - - - - - - - - - - - - - - -
void NeuralNetwork::propigateActivations(){
    //This is the magic formula given to feed forward in the network. Normalization can happen with either the
    //sigmoid "1 / (1 + exp(−x))" or the tanh "2σ(2x) − 1" function.
    float accumulator = 0;
    if((on == 1.0) && (off == 0.0)){
        for(int outerLoop = 0; outerLoop < hidUnits; outerLoop++){
            for (int innerLoop = 0; innerLoop < inUnits; innerLoop++){
                accumulator = accumulator + (nNlayersPtr->inputLayer.neurons[innerLoop] * nNlayersPtr->inputLayer.weights[innerLoop][outerLoop]);
            };
            accumulator = accumulator + (nNlayersPtr->inputLayer.neurons[inUnits] * nNlayersPtr->inputLayer.weights[inUnits][outerLoop]);
            nNlayersPtr->hiddenLayer.neurons[outerLoop] = (1 / (1 + pow(ee, -accumulator)));
            accumulator = 0;
        };
    }
    else if((on == 1.0) && (off == -1.0)){
        for(int outerLoop = 0; outerLoop < hidUnits; outerLoop++){
            for (int innerLoop = 0; innerLoop < inUnits; innerLoop++){
                accumulator = accumulator + (nNlayersPtr->inputLayer.neurons[innerLoop] * nNlayersPtr->inputLayer.weights[innerLoop][outerLoop]);
            };
            accumulator = accumulator + (nNlayersPtr->inputLayer.neurons[inUnits] * nNlayersPtr->inputLayer.weights[inUnits][outerLoop]);
            nNlayersPtr->hiddenLayer.neurons[outerLoop] = (2 / (1 + pow(ee, -2 * accumulator))) - 1;
            accumulator = 0;
        };
    }
    else{cout << "ERROR: Check config file for the correct values." << endl;};

    if((on == 1.0) && (off == 0.0)){
        for(int outerLoop = 0; outerLoop < outUnits; outerLoop++){
            for (int innerLoop = 0; innerLoop < hidUnits; innerLoop++){
                accumulator = accumulator + (nNlayersPtr->hiddenLayer.neurons[innerLoop] * nNlayersPtr->hiddenLayer.weights[innerLoop][outerLoop]);
            };
            accumulator = accumulator + (nNlayersPtr->hiddenLayer.neurons[inUnits] * nNlayersPtr->hiddenLayer.weights[hidUnits][outerLoop]);
            nNlayersPtr->outputLayer.neurons[outerLoop] = (1 / (1 + pow(ee, -accumulator)));
            accumulator = 0;
        };
    }
    else if((on == 1.0) && (off == -1.0)){
        for(int outerLoop = 0; outerLoop < outUnits; outerLoop++){
            for (int innerLoop = 0; innerLoop < hidUnits; innerLoop++){
                accumulator = accumulator + (nNlayersPtr->hiddenLayer.neurons[innerLoop] * nNlayersPtr->hiddenLayer.weights[innerLoop][outerLoop]);
            };
            accumulator = accumulator + (nNlayersPtr->hiddenLayer.neurons[inUnits] * nNlayersPtr->hiddenLayer.weights[hidUnits][outerLoop]);
            nNlayersPtr->outputLayer.neurons[outerLoop] = (2 / (1 + pow(ee, -2 * accumulator))) - 1;
            accumulator = 0;
        };
    }
    else{cout << "ERROR: Check config file for the correct values." << endl;};
}

//- - - - - - - - - - - - - - - - - - - - - - - -
//   ***CALCULATE ERROR IN THE OUTPUT LAYER***
//- - - - - - - - - - - - - - - - - - - - - - - -
void NeuralNetwork::computeOutputErrors(int indexValue){
    //The given formula to calculate the error in every neuron of the output layer.
    nNlayersPtr->outputLayer.errors[indexValue] = (nNlayersPtr->outputLayer.neurons[indexValue]) * (1 - nNlayersPtr->outputLayer.neurons[indexValue])
                                                * (outputData[indexValue][0] - nNlayersPtr->outputLayer.neurons[indexValue]);
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   ***CALCULATE ERROR IN THE HIDDEN LAYER NEURONS***
//- - - - - - - - - - - - - - - - - - - - - - - - - - - -
void NeuralNetwork::computeHiddenLayerErrors(){
    float accumulator = 0;
    for (int outerLoop = 0; outerLoop < (hidUnits + 1); outerLoop++){
        for (int innerLoop = 0; innerLoop < outUnits; innerLoop++){
            accumulator = accumulator + nNlayersPtr->outputLayer.errors[innerLoop] * nNlayersPtr->hiddenLayer.weights[outerLoop][innerLoop];
        };
        nNlayersPtr->hiddenLayer.errors[outerLoop] = (nNlayersPtr->hiddenLayer.neurons[outerLoop] * (1 - nNlayersPtr->hiddenLayer.neurons[outerLoop])) * accumulator;
    };
}

//- - - - - - - - - - - - - -
//   ***ADJUST WEIGHTS***
//- - - - - - - - - - - - - -
void NeuralNetwork::adjustWeights(){
    for (int outerLoop = 0; outerLoop < hidUnits; ++outerLoop) {
        for (int innerLoop = 0; innerLoop < outUnits; ++innerLoop) {
            nNlayersPtr->hiddenLayer.weights[outerLoop][innerLoop] = nNlayersPtr->hiddenLayer.weights[outerLoop][innerLoop] +
                                                                    (learnRate * nNlayersPtr->outputLayer.errors[innerLoop] *
                                                                    nNlayersPtr->hiddenLayer.neurons[outerLoop]);
        };
    };

    for (int outerLoop = 0; outerLoop < inUnits; ++outerLoop) {
        for (int innerLoop = 0; innerLoop < hidUnits; ++innerLoop) {
            nNlayersPtr->inputLayer.weights[outerLoop][innerLoop] = nNlayersPtr->inputLayer.weights[outerLoop][innerLoop] +
                                                                     (learnRate * nNlayersPtr->hiddenLayer.errors[innerLoop] *
                                                                      nNlayersPtr->inputLayer.neurons[outerLoop]);
        };
    };
}

//- - - - - - - - - - - - - - -
//   ***TRAIN THE NETWORK***
//- - - - - - - - - - - - - - -
void NeuralNetwork::train(){
    cout << endl << setw(65) << left << "                            Training                             " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    int iteration = 0;
    float meanSquaredError = 100000.0;
    while((iteration < maxEpoch) && (meanSquaredError != 0)){
        meanSquaredError = 0;
        for(int innerLoop = 0; innerLoop < ioPairs; innerLoop++){
            assignActivatons(innerLoop);
            propigateActivations();
            computeOutputErrors(0);
            meanSquaredError = meanSquaredError + (nNlayersPtr->outputLayer.neurons[innerLoop] - outputData[innerLoop][0]);
            computeHiddenLayerErrors();
            adjustWeights();
        };
        iteration++;
        meanSquaredError = pow(meanSquaredError, 2);
    };
    if(meanSquaredError == 0){cout << "                  ... Training stops early at epoch \"" << (iteration + 1) <<"\" due to the MSE." << endl;};
    cout << "                  ... Done." << endl;
}

//- - - - - - - - - - - - - - - - - - - - - -
//   ***USE THE NETWORK AFTER TRAINING***
//- - - - - - - - - - - - - - - - - - - - - -
void NeuralNetwork::test(){
    cout << endl << setw(65) << left << "                              Test                               " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    for(int iteration = 0; iteration < ioPairs; iteration++){
        assignActivatons(iteration);
        propigateActivations();
        cout << "                  " << nNlayersPtr->outputLayer.neurons[iteration] << endl;
    };
}

//- - - - - - - - - - - - - - - -
//   ***PRINT NEURON VALUES***
//- - - - - - - - - - - - - - - -
void NeuralNetwork::displayNeuronActivations(){
    cout << endl << setw(65) << left << "                     Neuron Activation Values                    " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    cout << "Input Layer:" << endl;
    for(int iteration = 0; iteration < inUnits; iteration++){
        cout << "                  " << nNlayersPtr->inputLayer.neurons[iteration] << endl;
    };
    cout << "                  " << nNlayersPtr->inputLayer.neurons[inUnits] << endl;
    cout << "Hidden Layer:" << endl;
    for(int iteration = 0; iteration < hidUnits; iteration++){
        cout << "                  " << nNlayersPtr->hiddenLayer.neurons[iteration] << endl;
    };
    cout << "                  " << nNlayersPtr->hiddenLayer.neurons[hidUnits] << endl;
    cout << "OutPut Layer:" << endl;
    for(int iteration = 0; iteration < outUnits; iteration++){
        cout << "                  " << nNlayersPtr->outputLayer.neurons[iteration] << endl;
    };
}

//- - - - - - - - - - - - - - - -
//   ***PRINT WEIGHT VALUES***
//- - - - - - - - - - - - - - - -
void NeuralNetwork::displayWeights(){
    cout << endl << setw(65) << left << "                          Weight Values                          " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    cout << "From Input To Hidden Layer:" << endl;
    for(int iteration = 0; iteration < inUnits; iteration++){
        for (int innerLoop = 0; innerLoop < hidUnits; innerLoop++) {
            cout << "[" << nNlayersPtr->inputLayer.weights[iteration][innerLoop] << "]";
        };
        cout << endl;
    };
    for (int loop = 0; loop < hidUnits; loop++) {
        cout << "[" << nNlayersPtr->inputLayer.weights[inUnits][loop] << "]";
    };
    cout << endl;

    cout << "From Hidden To Output Layer:" << endl;
    for(int iteration = 0; iteration < hidUnits; iteration++){
        for (int innerLoop = 0; innerLoop < outUnits; ++innerLoop) {
            cout << "[" << nNlayersPtr->hiddenLayer.weights[iteration][innerLoop] << "]";
        };
        cout << endl;
    };
    for (int loop = 0; loop < outUnits; ++loop) {
        cout << "[" << nNlayersPtr->hiddenLayer.weights[hidUnits][loop] << "]";
    };


}

//- - - - - - - - - - - - - - - - -
//   ***DISPLAY TRAINING DATA***
//- - - - - - - - - - - - - - - - -
void NeuralNetwork::displayTrainingInputOutput(){
    cout << endl << setw(65) << left << "                      Given Training Data                        " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    for(int iteration = 0; iteration < ioPairs; iteration++){
        cout << "                  [" << inputData[iteration][0] << "][" << inputData[iteration][1] << "]" << endl;
    };
    cout << endl << setw(65) << left << "                      Desired Output Data                        " << endl;
    cout << setw(65) << left << "                  - - - - - - - - - - - - - - -                  " << endl;
    for(int iteration = 0; iteration < ioPairs; iteration++){
        cout << "                  [" << outputData[iteration][0] << "]" << endl;
    };
}

//- - - - - - - - - - - -
//   ***CONSTRUCTOR***
//- - - - - - - - - - - -
NeuralNetwork::NeuralNetwork(){
    loadCfgParams();

    nNlayersPtr = new NNetSkeleton;
    buildInputLayer();
    buildHiddenLayer();
    buildOutputLayer();

    buildIOData();
}
