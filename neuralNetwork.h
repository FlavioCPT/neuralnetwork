/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: Final Project
 * Purpose: Network class definitions
 * Notes: The only functions not implemented was the one that were supposed
 *        to store weights in a file an the other that as neede to load
 *        weights from a file.
 * *************************************************************************/
#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include <cstdlib>   //For using the random() function.
#include <ctime>     //For using the system clock value as a seed in the "rand()".
#include <iomanip>   //For "left" and "setw()".
#include <fstream>   //For file operations.
#include <math.h>    //For "pow()".
#include <iostream>
#include <string>
using namespace std;


//- - - - - - - - - - - - - - - - - - - - - -
//   ***STRUCTURES TO SHAPE THE NETWORK***
//- - - - - - - - - - - - - - - - - - - - - -
//Input layer. Consists of activation values for "neurons" and "weights" branching to the hidden
//layer.
struct InLayer{
    float *neurons;
    float **weights;
};

//Hidden layer. Consists of activation values for "neurons" and "weights" branching to the
//output layer and "errors" values in the hidden neurons.
struct HidLayer{
    float *neurons;
    float **weights;
    float *errors;
};

//Output layer. Consists of activation values for "neurons" and "errors" values in the
//output neurons.
struct OutLayer{
    float *neurons;
    float *errors;
};

//The structure defining the whole neural network structure.
struct NNetSkeleton{
    InLayer inputLayer;
    HidLayer hiddenLayer;
    OutLayer outputLayer;
};


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//   ***FUNCTION COLLECTION TO WORK WITH THE NETWORK***
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class NeuralNetwork{
private:
    //Basic data for the network. These are loaded and initialized using the
    //"loadCfgParams()".
    float on;
    float off;
    float onSoft;
    float offSoft;
    int inUnits;
    int hidUnits;
    int outUnits;
    int maxEpoch;
    float learnRate;
    float ee;

    int ioPairs;
    //Double pointers to hold the training data set. The data set is loaded and
    //initialized in "buildIOData()".
    float **inputData;
    float **outputData;

    //A pointer to a "NNetworkSkeleton" struct which can access data in any of its three
    //inner structs. These are of course the "InLayer", "HidLayer", and "OutLayer" structs.
    NNetSkeleton *nNlayersPtr;

    //Set-up methods. They create and initialize the network and the network's data.
    float randomWeight();
    void loadCfgParams();
    void buildIOData();
    void buildInputLayer();
    void buildHiddenLayer();
    void buildOutputLayer();

    //These are the private training methods used in the public "train()" function.
    //"assignActivatons()" and "propigateActivations()" are also used in the "test()"
    //method.

    /* Teacher's Advice: depending on your implementation,
     * compute errors may also be used in the test method, but do not use
     * adjustWeights in the test method.*/
    void assignActivatons(int);
    void propigateActivations();
    void computeOutputErrors(int);
    void computeHiddenLayerErrors();
    void adjustWeights();
public:

    //COSTRUCTOR
    //Once an instance of this class is initiated, the network structure is built
    //calling the private functions set-up methods.
    NeuralNetwork();

    /* Teacher's code: These are just debug routines that help me see what's in the
     * network and to make sure it's working right. They do not add
     * to the functionality of the network.
     */
    void displayNeuronActivations();
    void displayWeights();
    void displayTrainingInputOutput();

    //This calls all the private training methods.
    void train();

    //This loads the validation set and tests the trained network.
    void test();

    //This saves the weights to a file when training is done.
    void saveweights();

    //This loads weights from a saved weights file so you can re-load
    //a trained network and use it, instead of retraining.
    void loadweights();
};
#endif //NEURALNETWORK_H
