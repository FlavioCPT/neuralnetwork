	After cloning all files to a local repo, the program can simply be compiled normally and run normally.

	In my case, with cygwin, I compile all ".cpp" files using the command "g++ -I ./ *.cpp -o prgmTest". Afterwords, The program is run using the command "./prgmTest.exe".

After compiling, One does not have to recompile to change the neural network's config data or training data. A simple change to the numerical values ONLY (no adding or subracting anything else to the file's elements) in the "config.txt" or "testData.txt" is sufficient. 