/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: Final Project
 * Purpose: Main program
 * Notes: NOT A WORKING PRODUCT. I must have done something wrong with the way
 *        that I calculated errors for every neuron because its not giving
 *        adequate outputs after training for any of the logic gates.
 * *************************************************************************/

#include "main.h"

int main(){
    //Here I seed my random number generator.
    srand(time(NULL));

    NeuralNetwork nn;

    nn.train();

    nn.displayNeuronActivations();
    nn.displayWeights();
    nn.displayTrainingInputOutput();

    nn.test();

   return 0;
}